import { AppointmentController } from "./controller/AppointmentController";
import { AppointmentBusiness } from "./model/AppointmentBusiness";
import { AppointmentRepository } from "./model/AppointmentRepository";
import { AppointmentView } from "./view/AppointmentView";




const controller = new AppointmentController(new AppointmentBusiness(new AppointmentRepository), new AppointmentView());
