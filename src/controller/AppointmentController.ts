
import { Appointment } from "../model/Appointment";
import type { AppointmentRepository } from "../model/AppointmentRepository";
import { IAppointmentBusiness } from "../model/IAppointmentBusiness";
import { AppointmentView } from "../view/AppointmentView";



export class AppointmentController {

    
    
    constructor(private business:IAppointmentBusiness, private view:AppointmentView){
       view.onFormSubmit = this.addAppointment.bind(this);
       view.renderList(this.business.displayAppointments());
    }

    addAppointment(date:string, time:string, label:string) {
        try {
            
            this.business.takeAppointment({date,time,label});
            this.view.renderList(this.business.displayAppointments());
        } catch (error) {
            //TODO: Afficher l'erreur dans la vue
            console.log(error);
        }
    }
    

    
}