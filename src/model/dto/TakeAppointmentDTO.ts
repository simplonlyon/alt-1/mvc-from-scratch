

export interface TakeAppointmentDTO {
    date:string;
    time:string;
    label:string;
}