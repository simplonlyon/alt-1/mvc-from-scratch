import { ListDisplayAppointmentDTO } from "./dto/ListDisplayAppointmentDTO";

const statusMap:Map<boolean,string> = new Map([
    [true, 'Accepted'],
    [false, 'Refused'],
    [null, 'Awaiting...'],
]);


export class Appointment {



    constructor(public label:string, 
        public date:Date, 
        public status?:boolean,
        public id?:number) {}

    validate() {
        this.status = true;
    }

    refuse() {
        this.status = false;
    }

    toListDisplay():ListDisplayAppointmentDTO {
        
            
        return {
            id:this.id,
            label:this.label,
            date:this.date.toLocaleString(),
            status: statusMap.get(this.status)
        }
    
    }


}