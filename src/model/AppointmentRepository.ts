import { Appointment } from "./Appointment";



export class AppointmentRepository {

    private appointments:Appointment[] = [
        new Appointment('defaut', new Date())
    ];
    private curIndex = this.appointments.length;

    add(appointment:Appointment):number {
        this.curIndex++;
        appointment.id = this.curIndex;
        this.appointments.push(appointment);
        return this.curIndex;
    }

    getAppointments() {
        return [...this.appointments];
    }

    delete(id:number) {
        this.appointments = this.appointments.filter(item => 
            item.id !== id);
    }

    
    


}